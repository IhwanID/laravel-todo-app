<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Session;

class TodoController extends Controller
{
    public function index(){
        //panggil data pakai use juga
        $todos = Todo::all();
        return view('pages.todo.index', compact('todos'));
    }

    public function store(Request $request){
        //dd($request->all());
        
        // method panjang
        // $todo = new Todo();
        // $todo->task = $request->get('task');
        // $todo->save();

        //method eloquent
        Todo::create($request->all());

        Session::flash('success', 'Your Task has been created!');

        return redirect()->back();
    }

    public function edit($id){
       $todo = Todo::findOrFail($id);
       Session::flash('success', 'Your Task has been edited!');
        return view('pages.todo.edit', compact('todo'));
    }

    public function update(Request $request, $id){
        $todo = Todo::findOrFail($id);
        $todo->task = $request->get('task');
        $todo->update();

        Session::flash('success', 'Your Task has been updated!');
        return redirect()->route('todo.index');
        
    }

    public function complete(Request $request, $id){
        $todo = Todo::findOrFail($id);
        $todo->status = true;
        $todo->update();
        
        Session::flash('success', 'Your Task has been completed!');
        return redirect()->route('todo.index');
        
    }

    public function destroy($id){
        $todo = Todo::findOrFail($id);
        $todo->delete();

        Session::flash('success', 'Your Task has been deleted!');
        return redirect()->route('todo.index');
        
    }
}
